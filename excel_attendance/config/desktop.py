from frappe import _

def get_data():
	return [
		{
			"module_name": "Excel Attendance",
			"type": "module",
			"label": _("Excel Attendance")
		}
	]
